<!DOCTYPE html>
<html lang='fr'>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>

<body>

    <?php

    // Faites un formulaire avec un input de type text, qui récupère une chaine de caractère.
    // Faites un autre champ qui récupère aussi une chaine de caractère
    // Lors de l'envoie du formulaire, la chaine contenue dans le deuxième champs se met en gras dans la première chaine et s'affiche
    // Compter aussi le nombre de fois que la chaine à été mis en gras
    ?>

    <!-- écrire le code après ce commentaire -->

    <form action="Exo4.php" method="get">
        <input type="text" name="chaine" placeholder="Texte à traiter">
        <input type="text" name="chaine1" placeholder="Mot à mettre en gras">
        <input type="submit" name="submit" value="Envoyer">
    </form>

    <?php

    if (isset($_GET['submit'])){

        $chaine = isset($_GET['chaine']) ? $_GET['chaine'] : "";
        $chaine1= isset($_GET['chaine1']) ? $_GET['chaine1'] : "";
    }

    function recupere($i,$j){

        $compter=substr_count($i,$j);
        $gras=str_replace($j,"<strong>".$j."</strong>",$i);
        
        echo "Le mot " .$j. " est present " .$compter. " fois, il est maintenant en gras <br>" ;

        echo $gras;
    }
        recupere($chaine,$chaine1);
 

    ?>
    <!-- écrire le code avant ce commentaire -->

</body>

</html>