<!DOCTYPE html>
<html lang='fr'>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>

<body>

    <?php

    //

    ?>

    <!-- écrire le code après ce commentaire -->

    <?php


    $perso = [
        ['perso' => 'Astérix', 'Est journaliste' => 'faux', 'A des couettes' => 'vrai', 'A un masque' => 'faux', 'Fait peur' => 'faux', 'Joue à la dînette' => 'faux'],
        ['perso' => 'Batman', 'Est journaliste' => 'faux', 'A des couettes' => 'faux', 'A un masque' => 'vrai', 'Fait peur' => 'vrai', 'Joue à la dînette' => 'faux'],
        ['perso' => 'Martine', 'Est journaliste' => 'faux', 'A des couettes' => 'vrai', 'A un masque' => 'faux', 'Fait peur' => 'faux', 'Joue à la dînette' => 'vrai'],
        ['perso' => 'Spider-Man', 'Est journaliste' => 'vrai', 'A des couettes' => 'faux', 'A un masque' => 'vrai', 'Fait peur' => 'vrai', 'Joue à la dînette' => 'faux'],
        ['perso' => 'Tintin', 'Est journaliste' => 'vrai', 'A des couettes' => 'faux', 'A un masque' => 'faux', 'Fait peur' => 'faux', 'Joue à la dînette' => 'faux']
    ];

    if (isset($_GET['choice']) && !empty($_GET['choice'])) {
        $choice = $_GET['choice'];
    }
    $i = 0;

    foreach ($perso as $line) { // Perso est le tableau, lit chaque ligne 1 par 1.
        if ($line[$choice] == 'vrai') { // Dans la ligne, si le choix 'Est journaliste' est vrai.
            $result[] = $line['perso']; // On stocke dans le tableau $result, la valeur liée à la clé 'perso'.
            $i++; // Incrémentation pour connaitre le nombre de personnage dans le tableau.
        }
    }

    echo 'Il y a ' . $i . ' personnage pour la question, c\'est : <br> ';
    foreach ($result as $valeur) { // Lit le tableau $result qui affiche la valeur contenu.
        echo $valeur . '<br>'; // 'Batman'.
    }

    ?>

    <form method="get">
        <input type="text" name="choice">
        <input type="submit" value="Envoyer">
    </form>
    <!-- écrire le code avant ce commentaire -->

</body>

</html>