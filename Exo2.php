<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php
    
             // La boucle est bouclée : 
             // Faites un script qui permet d'afficher un bloc, qui contient lui même 12 blocs.
             // Le premier bloc sera répété en fonction de $x
            
        ?>
            
        <!-- écrire le code après ce commentaire -->
            
                <?php
               
              $x=rand(1,5); 

              echo '<div class="content-mensualite">';
                for ($y = 1; $y <= $x; $y++) {

                    echo '<h4> Gros Bloc '.$y. '</h4><div class="align">';

                            for ($z = 0; $z < 13; $z++) {
                                echo '<div class ="mensualite">Bloc '.$y.' </div><br>';
                            }
                            echo '</div>';
                        }
                        echo '</div>';
                        
                ?>
            
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>